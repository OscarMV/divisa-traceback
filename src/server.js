const express = require('express');
const env = require('dotenv');
const request = require('request-promise');
env.config();

const server = express();

server.use(express.json());

server.set('port', process.env.SERVER_PORT || 8080);

server.listen(server.get('port'), async() => {
    console.log(`Server on port ${server.get('port')}`);
});

server.get('/', async(req, res) => {
    res.send('Test ok')
});

server.get('/:timestamp/:divisa/:amount', async(req, res) => {
    let crypto = ['BTC', 'ETH', 'LTC', 'XRP'];
    let normal = ['USD', 'GBP', 'CNY', 'RUB', 'EUR'];
    try {
        if((Date.now() - 7776000000) <= (req.params.timestamp * 1000)) {
            console.log('Todo ok');
            if(crypto.includes(req.params.divisa) || normal.includes(req.params.divisa)) {
                console.log('Todo ok x2');
                if(req.params.amount >= 0) {
                    let divisaTraceback = {};
                    divisaTraceback.date = new Date(req.params.timestamp * 1000);
                    let dateString = divisaTraceback.date.toISOString().slice(0, -14);
                    console.log('Todo ok x3');
                    if(crypto.includes(req.params.divisa)) {
                        divisaTraceback[crypto.splice(crypto.indexOf(req.params.divisa), 1)[0]] = Number(req.params.amount);
                        
                        let opt1 = {
                            uri: 'https://apiv2.bitcoinaverage.com/indices/global/history/'+ req.params.divisa +'USD?at='+ req.params.timestamp +'&resolution=hour',
                            headers: {
                                'X-Testing': 'testing',
                                'x-ba-key': 'ODNhZWQ2OTAxOWNhNDA5NWI2MWIxMjhmMjZhYzJiOTk'
                            },
                            method: 'GET',
                            json: true
                        };
                        let { average } = await request(opt1);
                        divisaTraceback[normal.splice(normal.indexOf('USD'), 1)[0]] = req.params.amount * average;

                        for (let element of crypto) {
                            let opt2 = {
                                uri: 'https://apiv2.bitcoinaverage.com/indices/global/history/'+ element +'USD?at='+ req.params.timestamp +'&resolution=hour',
                                headers: {
                                    'X-Testing': 'testing',
                                    'x-ba-key': 'ODNhZWQ2OTAxOWNhNDA5NWI2MWIxMjhmMjZhYzJiOTk'
                                },
                                method: 'GET',
                                json: true
                            };
                            let { average } = await request(opt2);
                            divisaTraceback[element] = divisaTraceback.USD / average;
                        }
                        
                        let symbol = normal.join(',');
                        let opt3 = {
                            uri: 'https://api.exchangeratesapi.io/history?start_at='+ dateString +'&end_at='+ dateString +'&symbols=' + symbol + '&base=USD',
                            method: 'GET',
                            json: true
                        };
                        console.log(opt3.uri);
                        let { rates } = await request(opt3);
                        let ref = rates[dateString];

                        for(element in ref) {
                            divisaTraceback[element] = ref[element] * divisaTraceback.USD;
                        }

                        console.log(divisaTraceback);

                        req.status(200).json({
                            'serverMessage': 'Ok',
                            divisaTraceback
                        });
                    } else {
                        divisaTraceback[normal.splice(normal.indexOf(req.params.divisa), 1)[0]] = Number(req.params.amount);
                        for (let element of crypto) {
                            let opt2 = {
                                uri: 'https://apiv2.bitcoinaverage.com/indices/global/history/'+ element + req.params.divisa +'?at='+ req.params.timestamp +'&resolution=hour',
                                headers: {
                                    'X-Testing': 'testing',
                                    'x-ba-key': 'ODNhZWQ2OTAxOWNhNDA5NWI2MWIxMjhmMjZhYzJiOTk'
                                },
                                method: 'GET',
                                json: true
                            };
                            console.log(opt2.uri);
                            let { average } = await request(opt2);
                            divisaTraceback[element] = divisaTraceback[req.params.divisa] / average;
                        }
                        let symbol = normal.join(',');
                        let opt3 = {
                            uri: 'https://api.exchangeratesapi.io/history?start_at='+ dateString +'&end_at='+ dateString +'&symbols=' + symbol + '&base=' + req.params.divisa,
                            method: 'GET',
                            json: true
                        };
                        console.log(opt3.uri);
                        let { rates } = await request(opt3);
                        let ref = rates[dateString];

                        for(element in ref) {
                            divisaTraceback[element] = ref[element] * divisaTraceback[req.params.divisa];
                        }

                        console.log(divisaTraceback);
                        res.send('owo');

                    }
                } else {
                    res.status(400).json({
                        'serverMessage': 'The amount must be 1 or higher'
                    });
                }
            } else {
                res.status(400).json({
                    'serverMessage': 'Currency target invalid'
                });
            }
        }
        else {
            res.status(400).json({
                'serverMessage': 'Date out of range'
            });
        }
    } catch(e) {
        res.send(e);
    }
});